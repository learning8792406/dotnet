﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LearnConsoleApp.basic
{
    internal class DataTypes
    {
        double numberOfHours = 5120.5;
        float hourlyRate = 60.5f;
        decimal income = 25399.65m;
        byte userAge = 25;
        int numberOfEmployees = 412;
    }
}
